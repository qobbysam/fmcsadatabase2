package dot

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	//"github.com/jinzhu/gorm"

	//gormbulkups "github.com/atcdot/gorm-bulk-upsert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

//var config gorm.Config = gorm.Config{}

type DBWriter struct {
	Abc     string
	Dsn     string
	DB      *gorm.DB
	Created bool
}

var Out DBWriter

func NewDBWriter(cof string) (*DBWriter, error) {

	//out := DBWriter{}

	if !Out.Created {

		DB, err := gorm.Open(postgres.Open(cof), &gorm.Config{CreateBatchSize: 200})

		if err != nil {
			return nil, err
		}
		Out.Dsn = cof
		Out.DB = DB
		Out.Created = true

		Out.DB.AutoMigrate(&ConstructInfo{}, &Fmcsa_object{})
	}

	return &Out, nil

}

//var Abc string = "test here"

//var dsn string =

type GenFmcsaInterface interface {
	GenFmcsa() Fmcsa_object
}

func StreamFmcsa(bulk []Additions) chan []interface{} {

	buffer_len := len(bulk) * 2

	out_list := make([]interface{}, 0)

	Out_message_chan := make(chan []interface{}, 2000)

	local_chan := make(chan interface{}, buffer_len)

	var wg sync.WaitGroup

	for _, value := range bulk {

		wg.Add(1)

		go func(out chan interface{}, line Additions) {

			//out_fmcsa := line.GenFmcsa()

			constrct_out, fmcsa_out := line.GenFmcsa()

			out <- constrct_out
			out <- fmcsa_out

			//out <- line.GenFmcsa()

			wg.Done()

		}(local_chan, value)

		//go func(out chan Fmcsa_object, line_in Additions)

		// out_fmcsa, out_construct := value.GenFmcsa()

		// Out_message_chan <- out_construct
		// Out_message_chan <- out_fmcsa

		//	wg.Done()

		//}//(Out_message_chan, value)

		wg.Add(1)
		go func(in chan interface{}, out_chan chan []interface{}, outL []interface{}) {

			for msg := range in {

				outL = append(outL, msg)

				if len(outL)%1000 == 0 {

					out_chan <- outL

					outL = nil
				}
			}

			out_chan <- outL

			wg.Done()

		}(local_chan, Out_message_chan, out_list)
	}

	defer close(local_chan)
	defer close(Out_message_chan)
	wg.Wait()
	return Out_message_chan
}

// func ProduceFmcsa(inlist []Additions) []GenFmcsaInterface {

// 	out := make([]GenFmcsaInterface, 0)

// 	for _, value := range inlist {

// 		out = append(out, value)

// 		//outfmcsa, outconstruct = append(out, value.GenFmcsa())
// 	}

// 	return out

// }

type CleanObjectInterface interface {
	CleanObject() []string
}
type ConstructInfo struct {
	//gorm.Model

	Dot_number int `gorm:"primaryKey"`

	First_time   bool
	Active       bool
	Check_count  int
	Update_count int

	Updates string `sql:"type:JSONB NOT NULL DEFAULT '{}'::JSONB"`

	Message string
}

const MyTimeFormat = "01JAN2001"

// type MyTime time.Time

// func NewMyTime(hour, min, sec int) MyTime {
// 	t := time.Date(0, time.January, 1, hour, min, sec, 0, time.UTC)
// 	return MyTime(t)
// }

// func (t *MyTime) Scan(value interface{}) error {
// 	switch v := value.(type) {
// 	case []byte:
// 		return t.UnmarshalText(string(v))
// 	case string:
// 		return t.UnmarshalText(v)
// 	case time.Time:
// 		*t = MyTime(v)
// 	case nil:
// 		*t = MyTime{}
// 	default:
// 		return fmt.Errorf("cannot sql.Scan() MyTime from: %#v", v)
// 	}
// 	return nil
// }

// // func ToMytime(date_in time.Time) MyTime{

// // 	out:= date_in

// // 	return MyTime(out)

// // }

// func (t MyTime) Value() (driver.Value, error) {
// 	return driver.Value(time.Time(t).Format(MyTimeFormat)), nil
// }

// func (t *MyTime) UnmarshalText(value string) error {
// 	dd, err := time.Parse(MyTimeFormat, value)
// 	if err != nil {
// 		return err
// 	}
// 	*t = MyTime(dd)
// 	return nil
// }

// func (MyTime) GormDataType() string {
// 	return "TIME"
// }

type Fmcsa_object struct {
	ConstructInfoDot_number int
	ConstructInfo           ConstructInfo `gorm:"foreignKey:ConstructInfoDot_number"`

	Dot_number  int `gorm:"primaryKey"`
	Mc_number   string
	Mc_add_date time.Time `gorm:"type:time"`

	Legal_name string

	Dba_name          string
	Carrier_operation string
	Dot_add_date      time.Time `gorm:"type:time"`

	Oic_state       string
	Nbr_power_units string
	Driver_total    string
	Hm_flag         string
	Pc_flag         string

	Phy_street  string
	Phy_city    string
	Phy_state   string
	Phy_zip     string
	Phy_country string

	Mailing_street  string
	Mailing_city    string
	Mailing_state   string
	Mailing_zip     string
	Mailing_country string

	Mcs150_date         string
	Mcs150_mileage_year string
	Mcs150_mileage      string

	Telephone     string
	Fax           string
	Email_address string
}

func (e *Fmcsa_object) GenFmcsa() {

}

func CreateDeletionFmcsa(inlist []string) (Fmcsa_object, ConstructInfo) {
	layout := "01JAN2001"

	date, err := time.Parse(layout, inlist[22])

	t := time.Now()

	var f_construct ConstructInfo

	var f_Fmcsa Fmcsa_object

	num, err := strconv.Atoi(inlist[0])
	if err != nil {
		fmt.Println("none here")
	}

	f_construct.Active = false
	f_construct.First_time = false
	//f_construct.Update_count = 0
	f_construct.Dot_number = num
	f_construct.Message = "deleted on here  " + t.String()

	f_Fmcsa.ConstructInfoDot_number = num

	f_Fmcsa.Dot_number = num
	f_Fmcsa.Legal_name = inlist[1]
	f_Fmcsa.Dba_name = inlist[2]
	f_Fmcsa.Carrier_operation = inlist[3]
	f_Fmcsa.Hm_flag = inlist[4]
	f_Fmcsa.Pc_flag = inlist[5]
	f_Fmcsa.Phy_street = inlist[6]
	f_Fmcsa.Phy_city = inlist[7]
	f_Fmcsa.Phy_state = inlist[8]
	f_Fmcsa.Phy_zip = inlist[9]
	f_Fmcsa.Phy_country = inlist[10]
	f_Fmcsa.Mailing_street = inlist[11]
	f_Fmcsa.Mailing_city = inlist[12]
	f_Fmcsa.Mailing_state = inlist[13]
	f_Fmcsa.Mailing_zip = inlist[14]
	f_Fmcsa.Mailing_country = inlist[15]
	f_Fmcsa.Telephone = inlist[16]
	f_Fmcsa.Fax = inlist[17]
	f_Fmcsa.Email_address = inlist[18]
	f_Fmcsa.Mcs150_date = inlist[19]
	f_Fmcsa.Mcs150_mileage = inlist[20]
	f_Fmcsa.Mcs150_mileage_year = inlist[21]
	f_Fmcsa.Dot_add_date = date
	f_Fmcsa.Oic_state = inlist[23]
	f_Fmcsa.Nbr_power_units = inlist[24]
	f_Fmcsa.Driver_total = inlist[25]

	return f_Fmcsa, f_construct
}

func CreateUpdateFmcsa(inlist []string) (Fmcsa_object, ConstructInfo) {

	layout := "01JAN2001"

	date, err := time.Parse(layout, inlist[22])

	var f_construct ConstructInfo

	var f_Fmcsa Fmcsa_object

	num, err := strconv.Atoi(inlist[0])
	if err != nil {
		fmt.Println("none here")
	}

	f_construct.Active = true
	f_construct.First_time = false
	//f_construct.Update_count = 0
	f_construct.Dot_number = num

	f_construct.Message = "old info : " + strings.Join(inlist[26:], ",")

	f_Fmcsa.ConstructInfoDot_number = num
	//f_Fmcsa.ConstructInfo = f_construct

	f_Fmcsa.Dot_number = num
	f_Fmcsa.Legal_name = inlist[1]
	f_Fmcsa.Dba_name = inlist[2]
	f_Fmcsa.Carrier_operation = inlist[3]
	f_Fmcsa.Hm_flag = inlist[4]
	f_Fmcsa.Pc_flag = inlist[5]
	f_Fmcsa.Phy_street = inlist[6]
	f_Fmcsa.Phy_city = inlist[7]
	f_Fmcsa.Phy_state = inlist[8]
	f_Fmcsa.Phy_zip = inlist[9]
	f_Fmcsa.Phy_country = inlist[10]
	f_Fmcsa.Mailing_street = inlist[11]
	f_Fmcsa.Mailing_city = inlist[12]
	f_Fmcsa.Mailing_state = inlist[13]
	f_Fmcsa.Mailing_zip = inlist[14]
	f_Fmcsa.Mailing_country = inlist[15]
	f_Fmcsa.Telephone = inlist[16]
	f_Fmcsa.Fax = inlist[17]
	f_Fmcsa.Email_address = inlist[18]
	f_Fmcsa.Mcs150_date = inlist[19]
	f_Fmcsa.Mcs150_mileage = inlist[20]
	f_Fmcsa.Mcs150_mileage_year = inlist[21]
	f_Fmcsa.Dot_add_date = date
	f_Fmcsa.Oic_state = inlist[23]
	f_Fmcsa.Nbr_power_units = inlist[24]
	f_Fmcsa.Driver_total = inlist[25]

	return f_Fmcsa, f_construct

}

func CreateNewFmcsa(inlist []string) (Fmcsa_object, ConstructInfo) {

	layout := "01JAN2001"

	date, err := time.Parse(layout, inlist[22])

	var f_construct ConstructInfo

	var f_Fmcsa Fmcsa_object

	num, err := strconv.Atoi(inlist[0])
	if err != nil {
		fmt.Println("none here")
	}

	f_construct.Active = true
	f_construct.First_time = true
	f_construct.Update_count = 0
	f_construct.Dot_number = num

	f_Fmcsa.ConstructInfoDot_number = num
	//f_Fmcsa.ConstructInfo = f_construct

	f_Fmcsa.Dot_number = num
	f_Fmcsa.Legal_name = inlist[1]
	f_Fmcsa.Dba_name = inlist[2]
	f_Fmcsa.Carrier_operation = inlist[3]
	f_Fmcsa.Hm_flag = inlist[4]
	f_Fmcsa.Pc_flag = inlist[5]
	f_Fmcsa.Phy_street = inlist[6]
	f_Fmcsa.Phy_city = inlist[7]
	f_Fmcsa.Phy_state = inlist[8]
	f_Fmcsa.Phy_zip = inlist[9]
	f_Fmcsa.Phy_country = inlist[10]
	f_Fmcsa.Mailing_street = inlist[11]
	f_Fmcsa.Mailing_city = inlist[12]
	f_Fmcsa.Mailing_state = inlist[13]
	f_Fmcsa.Mailing_zip = inlist[14]
	f_Fmcsa.Mailing_country = inlist[15]
	f_Fmcsa.Telephone = inlist[16]
	f_Fmcsa.Fax = inlist[17]
	f_Fmcsa.Email_address = inlist[18]
	f_Fmcsa.Mcs150_date = inlist[19]
	f_Fmcsa.Mcs150_mileage = inlist[20]
	f_Fmcsa.Mcs150_mileage_year = inlist[21]
	f_Fmcsa.Dot_add_date = date
	f_Fmcsa.Oic_state = inlist[23]
	f_Fmcsa.Nbr_power_units = inlist[24]
	f_Fmcsa.Driver_total = inlist[25]

	return f_Fmcsa, f_construct

}

func UpdateOrCreateFmcsa_Objects(list_in *[]interface{}) {

	// err = gormbulkups.BulkUpsert(DB, &list_in,300)

	// if err != nil {
	// 	fmt.Println("upsert failed")

	// }

	//in_list := make([]GenFmcsaInterface, 0)
	//in_list = list_in

	//DB.AutoMigrate()
	//i_list := make([]interface{}, 0)
	construct_list := make([]ConstructInfo, 0)
	fmcsa_list := make([]Fmcsa_object, 0)

	for _, value := range *list_in {

		switch value.(type) {

		case Fmcsa_object:
			fmcsa_list = append(fmcsa_list, value.(Fmcsa_object))

		case ConstructInfo:
			construct_list = append(construct_list, value.(ConstructInfo))

		default:
			continue
		}

		// valuex := reflect.ValueOf(value)

		// if valuex.Type().String() == "ConstructInfo"{

		// 	construct_list = append(construct_list, valuex

		// }

	}

	DB, err := GetDB()

	if err != nil {
		panic(fmt.Sprintln("db dissapeared", err))
	}
	DB.DB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&construct_list)

	DB.DB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&fmcsa_list)

	list_in = nil

	//fmt.Println("Commit good")
}

var GlobalConfig *StartConfig

func GetDB() (*DBWriter, error) {

	//fmt.Println("DSN HERE   ", GlobalConfig.Dsn)
	dsn := "host=localhost user=postgres password=moonlight dbname=go_red_2  port=5433 sslmode=disable TimeZone=Asia/Shanghai "
	db, err := NewDBWriter(dsn)

	return db, err
}
