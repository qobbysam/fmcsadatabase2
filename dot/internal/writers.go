package dot

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/dgraph-io/badger"
)

func ByteToList(byte_in *[]byte) []string {

	str_byte_in := string(*byte_in)

	if strings.Contains(str_byte_in, "####") {

		str_byte_in := string(*byte_in)

		out_main := strings.Split(str_byte_in, "####")

		original := strings.Split(out_main[0], "^")

		current := strings.Split(out_main[1], "^")

		//original = append(original, current...)

		current = append(current, original...)

		return current

	}

	out := strings.Split(str_byte_in, "^")

	return out

}

func ByteToListMod(byte_in *[]byte) []string {

	str_byte_in := string(*byte_in)

	out_main := strings.Split(str_byte_in, "####")

	original := strings.Split(out_main[0], "^")

	current := strings.Split(out_main[1], "^")

	original = append(original, current...)

	return original

}

// func streamNextKV(db_name string, out_buffersize int) (chan[][]string, error){

// 	lines := make([][]string, out_buffersize)

// 	db, err := badger.Open(badger.DefaultOptions(db_name))

// 	stream := db.NewStream()

// 	stream.Send = func(list *pb.KvListdb )

// }

func DiffWriter(bigName string, diffs Differences) error {

	//out_buffersize := 1000

	newDB := CreateNames(bigName)

	err := DiffWriterAdditions(newDB.AdditionDB, diffs.Additions)

	if err != nil {
		fmt.Println("failed to write additions")

		return err
	}

	err = DiffWriterModifications(newDB.ModificationDB, diffs.Modifications)

	if err != nil {
		fmt.Println("failed to write modifications")

		return err
	}

	err = DiffWriterDeletions(newDB.DeletionsDB, diffs.Deletions)

	if err != nil {
		fmt.Println("failed to write deletions")

		return err
	}

	return nil

}

func StreamKV(db_name string, out_buffersize int) (chan [][]string, error) {

	var mybuffer int = out_buffersize

	out_channel := make(chan [][]string, 20000)

	out_list := make([][]string, 0)

	counter := 0

	db, err := badger.Open(badger.DefaultOptions(db_name))

	if err != nil {
		fmt.Println("failed to open db")

		return out_channel, err
	}

	defer db.Close()
	defer close(out_channel)

	ok := db.View(func(txn *badger.Txn) error {

		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = mybuffer

		it := txn.NewIterator(opts)

		defer it.Close()

		for it.Rewind(); it.Valid(); it.Next() {

			counter = counter + 1

			//fmt.Println(len(it.))

			item := it.Item()
			//_ := item.Key()

			//value := item.Value()

			//fmt.Println("works here")

			err := item.Value(func(v []byte) error {

				out := ByteToList(&v)

				out_list = append(out_list, out)

				if len(out_list)%mybuffer == 0 {

					//fmt.Println("works here")

					out_channel <- out_list

					//fmt.Println("works here")

					out_list = nil

				}

				//fmt.Println("works here")

				//return out_channel, nil

				//out_list = nil
				return nil

			})

			if err != nil {

				fmt.Println(err)
				fmt.Println("end of file")
				//return err
			}

		}
		//fmt.Println(counter)

		return nil

	})

	out_channel <- out_list

	if ok != nil {
		fmt.Println("failed here in view")
	}

	//stream := db.NewStream()

	fmt.Println(counter)

	return out_channel, nil

}

func DiffWriterModifications(name_db string, add_list []Modifications) error {
	db, err := badger.Open(badger.DefaultOptions(name_db))

	//db,err := bolt.Open(name_bd,0600,nil )

	if err != nil {
		log.Fatal(err)
	}

	// txn := db.NewTransaction(true)

	// for _,v := range add_list{

	// 	err :=

	// }
	txn := db.NewWriteBatch()

	//outs := make([]dot.Additions, 0)

	for _, v := range add_list {

		//outs = append(outs, n)

		//if len(outs)%1000 == 0 {

		//for _, v := range outs {

		key := []byte(strconv.Itoa(int(v.Key)))

		line_value_original := v.Original

		line_value_current := v.Current

		oneLineOriginal := strings.Join(line_value_original[:], "^")
		oneLineCurrent := strings.Join(line_value_current[:], "^")

		oneLine := oneLineCurrent + "####" + oneLineOriginal

		//fmt.Println(v.Key)

		//fmt.Println(key)

		err := txn.Set(key, []byte(oneLine))
		// //_ = txn.Commit()

		if err == badger.ErrTxnTooBig {

			_ = txn.Flush()

			txn = db.NewWriteBatch()

			_ = txn.Set([]byte(strconv.Itoa(int(v.Key))), []byte(oneLine))

		}

		// if err != nil {
		// 	fmt.Println(err.Error())
		// 	fmt.Println("failed in set keys")
		// }

	}

	//outs = nil

	_ = txn.Flush()

	defer db.Close()

	return nil

}

func DiffWriterDeletions(name_db string, add_list []Deletions) error {
	db, err := badger.Open(badger.DefaultOptions(name_db))

	//db,err := bolt.Open(name_bd,0600,nil )

	if err != nil {
		log.Fatal(err)
	}

	// txn := db.NewTransaction(true)

	// for _,v := range add_list{

	// 	err :=

	// }
	txn := db.NewWriteBatch()

	//outs := make([]dot.Additions, 0)

	for _, v := range add_list {

		//outs = append(outs, n)

		//if len(outs)%1000 == 0 {

		//for _, v := range outs {

		key := []byte(strconv.Itoa(int(v.Key)))

		line_value := v.Value

		oneLine := strings.Join(line_value[:], "^")

		//fmt.Println(v.Key)

		//fmt.Println(key)

		err := txn.Set(key, []byte(oneLine))
		// //_ = txn.Commit()

		if err == badger.ErrTxnTooBig {

			_ = txn.Flush()

			txn = db.NewWriteBatch()

			_ = txn.Set([]byte(strconv.Itoa(int(v.Key))), []byte(oneLine))

		}

		// if err != nil {
		// 	fmt.Println(err.Error())
		// 	fmt.Println("failed in set keys")
		// }

	}

	//outs = nil

	_ = txn.Flush()

	defer db.Close()

	return nil

}

func DiffWriterAdditions(name_db string, add_list []Additions) error {

	db, err := badger.Open(badger.DefaultOptions(name_db))

	//db,err := bolt.Open(name_bd,0600,nil )

	if err != nil {
		log.Fatal(err)
	}

	// txn := db.NewTransaction(true)

	// for _,v := range add_list{

	// 	err :=

	// }
	txn := db.NewWriteBatch()

	//outs := make([]dot.Additions, 0)

	for _, v := range add_list {

		//outs = append(outs, n)

		//if len(outs)%1000 == 0 {

		//for _, v := range outs {

		key := []byte(strconv.Itoa(int(v.Key)))

		line_value := v.Value

		oneLine := strings.Join(line_value[:], "^")

		//fmt.Println(v.Key)

		//fmt.Println(key)

		err := txn.Set(key, []byte(oneLine))
		// //_ = txn.Commit()

		if err == badger.ErrTxnTooBig {

			_ = txn.Flush()

			txn = db.NewWriteBatch()

			_ = txn.Set([]byte(strconv.Itoa(int(v.Key))), []byte(oneLine))

		}

		// if err != nil {
		// 	fmt.Println(err.Error())
		// 	fmt.Println("failed in set keys")
		// }

	}

	//outs = nil

	_ = txn.Flush()

	defer db.Close()

	return nil
}
