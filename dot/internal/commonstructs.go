package dot

// func StreamDigests() (chan []Digest, chan error) {
// 	maxProcs := runtime.NumCPU()
// 	digestChannel := make(chan []Digest, bufferSize*maxProcs)
// 	errorChannel := make(chan error, 1)

// 	go func(digestChannel chan []Digest, errorChannel chan error) {
// 		wg := &sync.WaitGroup{}
// 		reader := csv.NewReader(FilePath)
// 		reader.Comma = e.config.Separator
// 		reader.LazyQuotes = e.config.LazyQuotes
// 		for {
// 			lines, eofReached, err := getNextNLines(reader)

// 			if err != nil {
// 				wg.Wait()
// 				close(digestChannel)
// 				errorChannel <- err
// 				close(errorChannel)
// 				return
// 			}

// 			wg.Add(1)
// 			go DigestForLines(lines, digestChannel, wg)

// 			if eofReached {
// 				break
// 			}
// 		}
// 		wg.Wait()
// 		close(digestChannel)
// 		errorChannel <- nil
// 		close(errorChannel)
// 	}(digestChannel, errorChannel)

// 	return digestChannel, errorChannel

// }

// func  DigestForLines(lines [][]string, digestChannel chan []Digest, wg *sync.WaitGroup) {
// 	output := make([]Digest, 0, len(lines))
// 	separator := string(e.config.Separator)
// 	for _, line := range lines {
// 		output = append(output, CreateDigest(line, separator, e.config.Key, e.config.Value))
// 	}

// 	digestChannel <- output
// 	wg.Done()
// }
