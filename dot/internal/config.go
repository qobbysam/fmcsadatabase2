package dot

type PreConfig struct {
	Mode    int
	FileOne string
	FileTwo string
}

type ConfigCsvCsv struct {
	CsvfileNameOne string
	CsvfileNameTwo string
}

func NewConfigCsvCsv(mode int, csvfileOne string, csvfileTwo string) *ConfigCsvCsv {

	return &ConfigCsvCsv{CsvfileNameOne: csvfileOne, CsvfileNameTwo: csvfileTwo}
}
