package dot

import (
	"fmt"
	"sync"
)

type DbNames struct {
	AdditionDB     string
	ModificationDB string
	DeletionsDB    string
}

func CreateNames(original string) DbNames {

	additions := original + "addditons"

	modification := original + "modifications"

	deletions := original + "deletions"

	return DbNames{AdditionDB: additions, ModificationDB: modification, DeletionsDB: deletions}
}

var WG sync.WaitGroup

func WriteMainDB(bigName string) error {

	out_buffersize := 1000

	newDB := CreateNames(bigName)

	WriteAddition(newDB.AdditionDB, out_buffersize)

	fmt.Println("Additions have been written succesfully")

	WriteModifications(newDB.ModificationDB, out_buffersize)

	fmt.Println("Modifications Sucess")

	WriteDeletion(newDB.DeletionsDB, out_buffersize)

	fmt.Println("Deletions Successful")

	return nil

	//WG.Wait()

}

func WriteModifications(dbName string, out_buffer_size int) {

	digests_main := make([]interface{}, 0)

	out, no := StreamKV(dbName, out_buffer_size)

	for msg := range out {

		for _, v := range msg {

			out_fmc, out_cons := CreateUpdateFmcsa(v)

			digests_main = append(digests_main, out_cons)

			digests_main = append(digests_main, out_fmc)

		}
		//WG.Add(1)
		//	go func() {
		UpdateOrCreateFmcsa_Objects(&digests_main)
		//	WG.Done()
		//	}()
		digests_main = nil
	}

	if no != nil {
		fmt.Println("Error streaming in modifications")
	}

	//	WG.Wait()

}

func WriteDeletion(dbname string, out_buffersize int) {

	digests_main := make([]interface{}, 0)

	out, no := StreamKV(dbname, out_buffersize)

	for msg := range out {

		for _, v := range msg {

			out_fmc, out_cons := CreateUpdateFmcsa(v)

			digests_main = append(digests_main, out_cons)

			digests_main = append(digests_main, out_fmc)

		}
		//WG.Add(1)
		//	go func() {
		UpdateOrCreateFmcsa_Objects(&digests_main)
		//	WG.Done()
		//}()
		digests_main = nil
	}

	if no != nil {
		fmt.Println("Error streaming in modifications")
	}
	WG.Wait()
}

func WriteAddition(dbName string, out_buffersize int) {

	//out_buffersize := 1000

	digests_main := make([]interface{}, 0)

	out, no := StreamKV(dbName, out_buffersize)

	for msg := range out {

		//fmt.Println("recevieng msg", len(msg))

		for _, v := range msg {

			out_fmc, out_cons := CreateNewFmcsa(v)

			digests_main = append(digests_main, out_fmc)
			digests_main = append(digests_main, out_cons)

			//fmt.Println("receiving here", v)

			// if len(digests_main)%out_buffersize == 0 {

			// 	UpdateOrCreateFmcsa_Objects(&digests_main)

			// 	digests_main = nil

			// }

		}

		// WG.Add(1)
		// go func() {
		UpdateOrCreateFmcsa_Objects(&digests_main)
		// WG.Done()
		// }()

		digests_main = nil

	}

	if no != nil {
		fmt.Println(no)
	}
	//WG.Wait()
}
