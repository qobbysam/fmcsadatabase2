package dot

import (
	"encoding/csv"
	"io"
	"sync"
)

var bufferSize int = BufferSize

func getNextNLines(reader *csv.Reader) ([][]string, bool, error) {
	lines := make([][]string, bufferSize)

	lineCount := 0
	eofReached := false
	for ; lineCount < bufferSize; lineCount++ {
		line, err := reader.Read()
		lines[lineCount] = line
		if err != nil {
			if err == io.EOF {
				eofReached = true
				break
			}

			return nil, true, err
		}
	}

	return lines[:lineCount], eofReached, nil
}

func digestForLines(lines [][]string, digestChannel chan []Digest, wg *sync.WaitGroup) {
	output := make([]Digest, 0, len(lines))
	//separator := string(e.config.Separator)
	for _, line := range lines {
		output = append(output, CreateDigest(line))
	}

	digestChannel <- output
	wg.Done()
}
