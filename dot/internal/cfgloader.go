package dot

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

type StartConfig struct {
	BaseCsv  string `json:"basecsv"`
	DeltaCsv string `json:"deltacsv"`
	Method   int    `json:"method"`
	Outname  string `json:"outname"`
	Dsn      string `json:"dsn"`
}

func NewStartConfig(basepath string) (*StartConfig, error) {

	outcfg := StartConfig{}
	config := filepath.Join(basepath, "config.json")

	out := StartConfig{}

	f2, err := os.Open(config)

	if err != nil {
		panic("Json not opened")
	}
	//err = viper.Unmarshal(&cfg)

	jsondec := json.NewDecoder(f2)

	err = jsondec.Decode(&outcfg)
	if err != nil {
		panic(fmt.Errorf("Fatal error dec9d8hg: %w \n", err))
	}

	out.BaseCsv = filepath.Join(basepath, outcfg.BaseCsv)
	out.DeltaCsv = filepath.Join(basepath, outcfg.DeltaCsv)
	out.Method = outcfg.Method
	out.Outname = outcfg.Outname

	return &out, nil

}
