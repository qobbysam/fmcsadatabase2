package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	dot "github.com/qobbysam/fmcsadatabase2/internal"
)

var start time.Time = time.Now()

func main() {

	//out_buffersize := 500

	//key_value_db := "new"

	//key_value_db_deletions := key_value_db + "deletions"

	//key_value_db_modifications := key_value_db + "modifications"
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)

	expath := filepath.Join(exPath, "data")

	startconfig, err := dot.NewStartConfig(expath)
	if err != nil {

	}

	dot.GlobalConfig = startconfig

	_, err = dot.GetDB()

	if err != nil {
		panic(fmt.Sprintf("db failed", err))
	}

	//base_csv := "data/empty.csv"

	base_csv := startconfig.BaseCsv

	//delta_csv := "data/nov2020.txt"

	delta_csv := startconfig.DeltaCsv

	//db_out_big := "sample4"
	db_out_big := startconfig.Outname
	//method := 1

	method := startconfig.Method
	config_L := dot.NewConfigCsvCsv(method, base_csv, delta_csv)

	fmt.Println(*config_L)

	//differences, err := dot.Diff(*config_L)

	out_dif, err := dot.Diff(*config_L)

	msg := fmt.Sprintf("additions total is: ", len(out_dif.Additions), "Modifications totalt is: ", len(out_dif.Modifications), "Deletions total is:  ", len(out_dif.Deletions))
	// fmt.Println()
	// fmt.Println()

	if err != nil {
		fmt.Println("error failed")
	}

	ok := dot.DiffWriter(db_out_big, out_dif)

	out_dif = dot.Differences{}

	if ok != nil {
		fmt.Println("main diff writer failed")
	}

	now := time.Now()

	fmt.Println("diff itime:  ", now.Sub(start))

	fmt.Println("starting write db\nxxxxxxxx\nxxxxxxx\nxxxxxxxx")

	ok = dot.WriteMainDB(db_out_big)

	if ok != nil {
		fmt.Println("main db writer failed")
	}

	now = time.Now()

	fmt.Println("diff itime:  ", now.Sub(start))

	fmt.Println("additions total is: ", len(out_dif.Additions))
	fmt.Println("Modifications totalt is: ", len(out_dif.Modifications))
	fmt.Println("Deletions total is:  ", len(out_dif.Deletions))

	fmt.Println(msg)
	// if err != nil {
	// 	fmt.Println("error here", err)
	// }

	// fmt.Println("len additions:  ", len(differences.Additions))
	// fmt.Println("len Modifications:  ", len(differences.Modifications))
	// fmt.Println("len deleted:  ", len(differences.Deletions))

	// fmt.Println("test all good")

	//fmt.Println(dot.Abc)

}

// func main() {

// }
